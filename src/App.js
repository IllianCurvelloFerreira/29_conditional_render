import "./App.css";
import { Component } from "react";
import ProductList from "./Components/ProductList/ProductList";

class App extends Component {
  state = {
    productList: [
      {
        name: "Doce de abóbora",
        price: 0.5,
        discountPercentage: null,
      },
      {
        name: "Salgadinho",
        price: 2.5,
        discountPercentage: 10,
      },
      {
        name: "Refrigerante",
        price: 8.5,
        discountPercentage: 5,
      },
      {
        name: "Maçã",
        price: 0.7,
        discountPercentage: null,
      },
      {
        name: "Feijão",
        price: 2.7,
        discountPercentage: 15,
      },
    ],
  };
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.state.productList.map((produto) => (
            <ProductList item={produto}></ProductList>
          ))}
        </header>
      </div>
    );
  }
}

export default App;
