import { Component } from "react";
import Sale from "../Sale/Sale";

class ProductList extends Component {
  render() {
    const { name, price, discountPercentage } = this.props.item;
    return (
      <>
        <h1>{name}</h1>
        <b>{price}</b>
        <Sale>{discountPercentage}</Sale>
      </>
    );
  }
}

export default ProductList;
